//var storage = Window.localStorage;

function getLocalArray(local){	
	array =  JSON.parse(localStorage.getItem(local));	
	if (array == null) {
		array = [];   
		localStorage.setItem(local, JSON.stringify(array));
	} 	
	return array;
}

function setLocalArray(local, arr){
		localStorage.setItem(local, JSON.stringify(arr));
}

function width3(no) {
  var s = "000" + no;
  return s.substr(s.length-3);
}

function juzOf(page) {
	if(page<22) 
		return 1;
	else 
		return Math.floor( (page-2)/20 + 1 );
}

function surahOf(page){
  for (var n=0; n<surah.length; n++) {
	if (page == surah[n]['idx'])
			return surah[n]['id'];
    if (page < surah[n]['idx'])
			return surah[n-1]['id'];
  }	
}

function gen_surah_rows(){
	//generate surah rows
	var i=0, rows=''; 
	for (row=0; row<38; row++){
	
	  var cols =''; 
	  var ii =[0,38,76];
	  for (col=0; col<3; col++){
		i = row + ii[col];

		cols = cols + 
		'<div class="col">'+
			  '<a data-idx="'+surah[i]["idx"]+'" href="#"><div class="card">'
			  +surah[i]["no"]+'. '+surah[i]["id"]+' - '+surah[i]["ar"]+' - p'+surah[i]["idx"]+'</div></a>'+
		 '</div>';
		 //i++;
	  }    
	  
	  cols = '<div class="row">'+cols+'</div>'
	  rows = rows+cols;
	}
	
	
	
	u('#tab-1').append(rows);
};

function gen_juz_rows() {
	//generate juz rows
	var i=0, rows=''; 
	for (row=0; row<10; row++){
	  var cols =''; 
	  for (col=0; col<3; col++){
		cols = cols + 
		'<div class="col">'+
			  '<a data-idx="'+juz[i]["idx"]+'" href="#"><div class="card"><h4>Juz '+juz[i]["no"]+'.</h4>'+
			  juz[i]["sur"]+' ('+juz[i]["nos"]+' : '+juz[i]["aya"]+') '+'</div></a>'+
		 '</div>';
		 i++;
	  }    
	  cols = '<div class="row">'+cols+'</div>'
	  rows = rows+cols;
	}
	u('#tab-2').append(rows);
}

function gen_bookmark_rows(){
	//generate bookmarks row
	var p, i=0, rows=''; 
    var bookmarks = getLocalArray("bookmarked-pages");
       
   	while (bookmarks.length < 12) bookmarks.push(0);
	
	//add extra rows for bookmark more than 9. 	
	for (row=0; row<4; row++){
	  var cols =''; 
	  for (col=0; col<3; col++){
		p = bookmarks[i];
		if ( p > 0 ) {				
			cols = cols + 
			'<div class="col-3">'+
				  '<a data-idx="'+p+'" href="#"><div class="card">'+ 'Hal '+p+', '+surahOf(p)+', Juz '+juzOf(p) +'</div></a>'+
			 '</div>'+
			 '<div class="col-1"><a data-idx="'+p+'" href="#"><i class="del icono-crossCircle"></i></a></div>';
		} else {
			cols = cols + '<div class="col-3"><a data-idx="0" href="#" class="hide"><div class="card"></div></a></div><div class="col-1"><a data-idx="0" href="#" class="hide"><i class="del icono-crossCircle"></i></a></div>';
		}
		i++;
	  }    
	  cols = '<div class="row">'+cols+'</div>'
	  rows = rows+cols;
	}		
	u('#tab-3 div').remove();
	u('#tab-3').append(rows);
}

function bookmarkDel(idx) {
	var data = parseInt(idx);
	var bookmarks = getLocalArray("bookmarked-pages");
	var index = bookmarks.indexOf(data);
	bookmarks.splice(index, 1);
	
	console.log(bookmarks);    
	setLocalArray("bookmarked-pages", bookmarks);
}

function bookmarkAdd(idx) {
	
	var data = parseInt(idx);
    var bookmarks = getLocalArray("bookmarked-pages");
        
    if (bookmarks.indexOf(data) == -1) {
		bookmarks.push(data);
		
		if (bookmarks.length>9) bookmarks.shift();
		
		console.log(bookmarks);    
    
		setLocalArray("bookmarked-pages", bookmarks);
		return true;
    } else {
		return false;
	}    
}

function activateCurrentTab(){
	var tab = getLocalArray("current-tab");
	
	if (tab.length == 0) 
		tab.push("#tab-1");
	
	u('div.tabs a').each(function(node){
		if ( u(node).attr("href")==tab[0] )
			u(node).addClass("active").siblings().removeClass("active");
	});
	
	u(tab[0]).addClass('active').siblings().removeClass("active");
}


u('div.tabs a').on('click', function(e){	
	e.preventDefault(); 
	u(this).addClass("active").siblings().removeClass("active");
	
	var thisHref = [];
	thisHref[0] = u(this).attr('href');		
	u(thisHref[0]).addClass('active').siblings().removeClass("active");
	
	setLocalArray("current-tab", thisHref);	
});


var pswpElement = document.querySelectorAll('.pswp')[0];

// build qur'an pages array
var pages = [
    {
        src: 'p/page604.png',
        w: 1024,
        h: 1656,
        pid: 'hal-604',
        title: 'Hal 604, Al Ikhlash, Juz 30'
    }
];

for (p=603; p>0; p--){
	pages.push(
      {
        src: 'p/page'+width3(p)+'.png',
        w: 1024,
        h: 1656,
        pid: 'hal-'+p,
        title: 'Hal '+p+', '+surahOf(p)+', Juz '+juzOf(p)
      }	
	);
}

//initializing...
gen_surah_rows();
gen_juz_rows();
gen_bookmark_rows();
activateCurrentTab();

var options = {
	'index': 0,
	'captionEl': true,
	'history': true,
	'shareEl': true,
	shareButtons: [
        {label:'Bookmark'}
    ]
}

u('button.pswp__button--share').on('click',function(e){
	e.preventDefault();
	var msg=u('#pswp__item_2').find('img').attr('src');
	
	var hash = window.location.hash;
	var idx =  hash.split("-").pop();

	if ( bookmarkAdd(idx) ) {
		var p = parseInt(idx);
		var caption = 'Hal '+p+', '+surahOf(p)+', Juz '+juzOf(p);
		
		u( u('div.col-3 > a.hide').first() ).data('idx', idx);
		u( u('div.col-3 > a.hide > div.card').first() ).text(caption);
		u( u('div.col-3 > a.hide').first() ).removeClass("hide");
		
		u( u('div.col-1 > a.hide').first() ).data('idx', idx);
		u( u('div.col-1 > a.hide').first() ).removeClass("hide");
	}
	//gen_bookmark_rows();
});

u('a i.del').on('click', function(e){
	e.preventDefault();
	var idx = u(this).closest('a').data('idx');
	bookmarkDel(idx);
	//gen_bookmark_rows();
	u('div.row a').each(function(node, i){
		if ( u(node).data("idx") == idx )
			u(node).addClass("hide");
	});
});


u('a div.card').on('click', function(e){
	e.preventDefault();
	var idx = u(this).closest('a').data('idx');
	options.index = 604 - idx;
	var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, pages, options );

   gallery.listen('imageLoadComplete', function(index, item) { 
       // index - index of a slide that was loaded
       // item - slide object
	   gallery.applyZoomPan(1, gallery.viewportSize.x/2-512, 0);
   });

	gallery.init();
});


var input_goto = document.getElementById("input_goto");
input_goto.addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        document.getElementById("button_goto").click();
    }
});


u('#button_goto').on('click', function(e){
	e.preventDefault();
	var idx = document.getElementById("input_goto").value;
	if (idx > 604) idx = 604;
	options.index = 604-idx;
	var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, pages, options );
   gallery.listen('imageLoadComplete', function(index, item) { 
       // index - index of a slide that was loaded
       // item - slide object
	   gallery.applyZoomPan(1, gallery.viewportSize.x/2-512, 0);
   });
	gallery.init();
});	

// Get the modal
var modal = document.getElementById('aboutModal');

// Get the button that opens the modal
var btn = document.getElementById("button_info");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
