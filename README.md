**Quran Offline**

Our ustadz need to show Al-Qur'an pages on projector screen (via his laptop). But our school has no internet connection yet.

So I create this project to show Al-Quran pages on laptop/computer without internet connection.

Installation:

Just Download this repository, extract on disk, and use get-pages.sh to download Qur'an pages to directory "p". This script has only test on linux. For other OS please modified it, or download pages manually.


Or if you just want to download, extract and run (open index.html), download all needed file here:
- https://www.dropbox.com/s/dvkhmnuogjf2whf/quran-off.zip


This project is built with:

* chota css
* icono css
* umbrella js
* photoswipe js
* quran.com



Risiyanto 

+ https://facebook.com/risiyanto
+ https://twitter.com/risiyanto